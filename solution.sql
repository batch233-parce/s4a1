====== ACTIVITY ======

-- Artists that has letter d in their names
SELECT * FROM artists WHERE name LIKE "%d%";

-- Songs that has less than 2:30
SELECT * FROM songs WHERE length < 230;

-- Joining album and song tables only showing album name, song name, and length
SELECT album_title, title, length FROM albums JOIN songs ON albums.id = songs.album_id;

-- Joining artist and album tables and find albums with letter "a"
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- Sorting abum in descending order and limit to 4
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Joining album and song tables and sort in descending order
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;